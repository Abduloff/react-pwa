import React, {Component} from 'react';
import {BrowserRouter, Route, Link} from 'react-router-dom';
import styled from 'styled-components'
import './Router.scss';

const StyledNavbar = styled.div`
    background-color: #4169e1;
    `;

const NavbarLink = styled(Link)`
  display: inline-block;
  padding: 10px;
  color: #fff;
  text-decoration: none;
`;

const NavbarTitle = styled.h3`
  display: inline-block;
  text-align: left;
  padding: 10px;
  color: black;
  text-decoration: none;
  `;

const Template = (props) => (
  <div>
    <StyledNavbar>
      <NavbarTitle>Task Manager</NavbarTitle>
      <NavbarLink to="/">Current Tasks</NavbarLink>
      <NavbarLink to="/completed">Completed Tasks</NavbarLink>
    </StyledNavbar>
    <p className="page-info">
      {props.title}:
    </p>
    <ul className={props.status}>
      <li>Task 1</li>
      <li>Task 2</li>
      <li>Task 3</li>
    </ul>
  </div>
);

const CurrentTasks = () => (
  <Template title="Current Tasks" status="Current"/>
);

const CompletedTasks = () => (
  <Template title="Completed Tasks" status="Completed"/>
);

export default class Router extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Route exact path="/" component={CurrentTasks}/>
          <Route path="/completed" component={CompletedTasks}/>
        </div>
      </BrowserRouter>
    );
  }
}