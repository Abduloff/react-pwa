self.addEventListener('install', function(e) {
  e.waitUntil(
    caches.open('v01').then(function(cache) {
      return cache.addAll([
        '/',
        '/index.html',
        '/static/js/bundle.js',
        '/static/js/0.chunk.js',
        '/static/js/main.chunk.js',
        '/sockjs-node',
        '/manifest.json',
        '/favicon.ico',
        '/app-icon.png',
        '/worker',
      ]);
    })
  );
});


self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request).then(function(response) {
      return response || fetch(event.request);
    })
  );
});